package com.lurit.nofrks.buildingapp.BApp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(BAppApplication.class, args);
		final Logger LOGGER = LoggerFactory.getLogger(BAppApplication.class);
		System.out.println("BApp started");
		LOGGER.info("app stated...");
		
	}

}

